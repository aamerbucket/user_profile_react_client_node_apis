const express = require('express');
const register_user = require('./routers/register_user');
const login_user = require('./routers/login_user');
const cors = require('cors');
const bodyParser = require('body-parser');
require('./db/db');
const app = express();
const port = process.env.PORT;

app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({
    extended: false
}))
app.use('/users', register_user);
app.use('/users', login_user);

app.listen(port, ()=>{
    console.log(`server running on port ${port}`);
});