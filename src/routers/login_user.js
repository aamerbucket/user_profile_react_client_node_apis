const express = require('express')
const {User} = require('../models/User')
const router = express.Router()
const Joi = require('joi');
const bcrypt = require('bcrypt');
const authorization = require('../middleware/auth');
const _ = require('lodash');
// register
router.post('/login', async (req, res)=>{
    // if request is empty, then send bad request
    const { error } = validate(req.body);
    if(error){
        return res.status(400).send(error.details[0].message);
    }
    try {
        const email = req.body.email;
        const password = req.body.password;
        const user = await User.findByCredentials(email, password)
        if (!user) {
            return res.status(401).send({error: 'Login failed! Check authentication credentials'})
        }
        const token = await user.generateAuthToken()
        //res.send({ user, token })
        res.send(token);
    } catch (error) {
        res.status(400).send(error)
    }
});

// access user profile
router.get('/profile', authorization, async(req, res) => {
    // View logged in user profile
    try {
        // here we are taking the id from token payload
        const user = await User.findById(req.user._id).select('-password');
        if(!user){
            res.send('User does not exists.')
        }
        res.send(user);
        } catch (error) {
            res.send(error.message);
    }
})

function validate(req){
    const schema = {
        email: Joi.string().min(5).max(50).required().email(),
        password: Joi.string().min(5).max(255).required()
    };
    return Joi.validate(req, schema)
}

module.exports = router