const express = require('express')
const {User, validate} = require('../models/User')
const router = express.Router()
const bcrypt = require('bcrypt');
const authorization = require('../middleware/auth');
const _ = require('lodash');
// register
router.post('/register', async (req, res)=>{
    // if request is empty, then send bad request
    const { error } = validate(req.body);
    if(error){
        return res.status(400).send(error.details[0].message);
    }
    // if email already exists, then send please login
    let user = await User.findOne({ email: req.body.email });
        if(user){
            return res.status(400).send('User already registered...!');
        }
    user = new User(_.pick(req.body, ['name', 'email', 'password']));
    try {
        const token = user.generateAuthToken();
        await user.save();
        res.header('x-auth-token', token).send(_.pick(user, ['_id', 'name', 'email']));
    } catch (error) {
        res.send(error.message)
    }
});

module.exports = router